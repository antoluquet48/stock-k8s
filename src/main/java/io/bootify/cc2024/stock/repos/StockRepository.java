package io.bootify.cc2024.stock.repos;

import io.bootify.cc2024.stock.domain.Stock;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;


public interface StockRepository extends JpaRepository<Stock, UUID> {

    boolean existsByNameIgnoreCase(String name);

}
