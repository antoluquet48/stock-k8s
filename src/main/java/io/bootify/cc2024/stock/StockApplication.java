package io.bootify.cc2024.stock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class StockApplication {

    public static void main(final String[] args) {
        SpringApplication.run(StockApplication.class, args);
    }

}
