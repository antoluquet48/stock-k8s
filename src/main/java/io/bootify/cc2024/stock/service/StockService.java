package io.bootify.cc2024.stock.service;

import io.bootify.cc2024.stock.domain.Stock;
import io.bootify.cc2024.stock.model.StockDTO;
import io.bootify.cc2024.stock.repos.StockRepository;
import io.bootify.cc2024.stock.util.NotFoundException;
import java.util.List;
import java.util.UUID;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


@Service
public class StockService {

    private final StockRepository stockRepository;

    public StockService(final StockRepository stockRepository) {
        this.stockRepository = stockRepository;
    }

    public List<StockDTO> findAll() {
        final List<Stock> stocks = stockRepository.findAll(Sort.by("id"));
        return stocks.stream()
                .map(stock -> mapToDTO(stock, new StockDTO()))
                .toList();
    }

    public StockDTO get(final UUID id) {
        return stockRepository.findById(id)
                .map(stock -> mapToDTO(stock, new StockDTO()))
                .orElseThrow(NotFoundException::new);
    }

    public UUID create(final StockDTO stockDTO) {
        final Stock stock = new Stock();
        mapToEntity(stockDTO, stock);
        return stockRepository.save(stock).getId();
    }

    public void update(final UUID id, final StockDTO stockDTO) {
        final Stock stock = stockRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        mapToEntity(stockDTO, stock);
        stockRepository.save(stock);
    }

    public void delete(final UUID id) {
        stockRepository.deleteById(id);
    }

    private StockDTO mapToDTO(final Stock stock, final StockDTO stockDTO) {
        stockDTO.setId(stock.getId());
        stockDTO.setName(stock.getName());
        stockDTO.setQualitity(stock.getQualitity());
        return stockDTO;
    }

    private Stock mapToEntity(final StockDTO stockDTO, final Stock stock) {
        stock.setName(stockDTO.getName());
        stock.setQualitity(stockDTO.getQualitity());
        return stock;
    }

    public boolean nameExists(final String name) {
        return stockRepository.existsByNameIgnoreCase(name);
    }

}
