package io.bootify.cc2024.stock.model;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.util.UUID;


public class StockDTO {

    private UUID id;

    @NotNull
    @Size(max = 255)
    @StockNameUnique
    private String name;

    @NotNull
    private Integer qualitity;

    public UUID getId() {
        return id;
    }

    public void setId(final UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Integer getQualitity() {
        return qualitity;
    }

    public void setQualitity(final Integer qualitity) {
        this.qualitity = qualitity;
    }

}
