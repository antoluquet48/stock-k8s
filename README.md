# Stock
## Instalar BD postgres con Helm Chart

https://github.com/bitnami/charts/tree/main/bitnami/postgresql/#installing-the-chart

### Postgres
Dentro de la carpeta "helm-chat", ejecute los siguientes comandos
```
helm repo add bitnami https://charts.bitnami.com/bitnami
helm install helm-postgres bitnami/postgresql  -f values.yaml
```
Luego, cree la base de datos "stock" en Postgres y manténgala con el usuario "postgres".

## Crear registro secreto privado
```
kubectl create secret docker-registry gitlab /
--docker-server=registry.gitlab.com /
--docker-username=<gitlab-user> /
--docker-password= <gitlab-pass>
```

## Implementar Infraestructura
Dentro de la carpeta "infra", ejecute los siguientes comandos
```
kubectl apply -f .\configMap.yaml
kubectl apply -f .\deployment.yaml
kubectl apply -f .\service.yaml
kubectl apply -f .\virtualservice.yaml
```

## Crear y enviar una imagen al Registry
```
mvnw spring-boot:build-image
docker tag docker.io/library/<name-image>  registry.gitlab.com/<user>/<proyect>:tag
docker push registry.gitlab.com/<user>/<proyect>:tag
```


